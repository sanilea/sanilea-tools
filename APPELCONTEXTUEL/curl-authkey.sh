
## ENV
#source curl.env 
server="na-demo-speedcall.itineraire.app"
login=""
password=""
auth_key=""

#------------------------------------------
#
# AC - formulaire demande transport
#
#------------------------------------------
	## ENV
	type=patient

	## - login/pass
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&login=${login}&password=${password}&nom=NOM&prenom=Prenom&nirpp=158193554600168&date_naissance=01%2F10%2F1958&adresse_1=10%20impasse%20de%20la%20voilact%C3%A9e&adresse_2=rue%20bis&code_postal=87000&ville=LIMOGES"

	## - auth_key
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&auth_key={authkey}&nom=NOM&prenom=Prenom&nirpp=158193554600168&date_naissance=01%2F10%2F1958&adresse_1=10%20impasse%20de%20la%20voilact%C3%A9e&adresse_2=rue%20bis&code_postal=87000&ville=LIMOGES"


#------------------------------------------
#
# AC - formulaire prescription
#
#------------------------------------------
        ## ENV
        type=prescription

        ## - login/pass
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&login=${login}&password=${password}&nom=NOM&prenom=Prenom&nirpp=158193554600168&date_naissance=01%2F10%2F1958&adresse_1=10%20impasse%20de%20la%20voilact%C3%A9e&adresse_2=rue%20bis&code_postal=87000&ville=LIMOGES"

        ## - auth_key
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&auth_key={authkey}&nom=NOM&prenom=Prenom&nirpp=158193554600168&date_naissance=01%2F10%2F1958&adresse_1=10%20impasse%20de%20la%20voilact%C3%A9e&adresse_2=rue%20bis&code_postal=87000&ville=LIMOGES"


#------------------------------------------
#
# AC - liste des missions
#
#------------------------------------------
        ## ENV
        type=transport

        ## - login/pass
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&login=${login}&password=${password}"

        ## - auth_key
curl https://${server}/api/contextual_call/transport_request_create --data "type=${type}&auth_key={authkey}"
