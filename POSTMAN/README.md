# Utilisation des librairies POSTMAN
## Environnement
le fichier  `envX.postman_environment.json` peut -être importé et dupliqué plusieurs fois afin de disposer de plusieurs environnements , c’est à dire un environnement de test par exemple et un autre pour la plateforme de démo, … 

les variables à reprendre dans les scripts sont par exemple:
* server
	* valeurs : na-demo-speedcall.itinraire.app, preprod.itineraire.sanilea.tech, …
* login
* password
* authkey
* jwt_token
* …

## Collections
vous trouverez des collections permettant de tester certaines interfaces comme l’appel contextuel et les apis
les noms des collections sont généralement du format suivant:
	`{plateforme}-{type}.postman_collection.json`
